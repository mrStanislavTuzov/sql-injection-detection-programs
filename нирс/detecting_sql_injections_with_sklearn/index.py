import numpy as np
import pandas as pd

from normalization import get_normalized_string

data = pd.read_csv("../data/data.csv")
data.loc[data["label"]=="norm", "label",] = 1
data.loc[data["label"]=="anom", "label",] = 0

# preliminary_processing - предварительная обработка
data["preliminary_processing"] = data["payload"].apply(get_normalized_string)

from sklearn.feature_extraction.text import CountVectorizer
vect = CountVectorizer()
X = vect.fit_transform(data["preliminary_processing"])
columns=vect.get_feature_names()
df = pd.DataFrame(X.A, columns=columns)
print(df)

# нормализация
from numpy import set_printoptions
from sklearn import preprocessing
from sklearn.preprocessing import Normalizer
Data_normalizer = Normalizer(norm="l1").fit(df.values)
Data_normalized = Data_normalizer.transform(df.values)
set_printoptions(precision=2)

# разделим данные на обучающую и тестовую выборки
from sklearn.model_selection import train_test_split
label_values = np.array(list(data["label"].values))
x_train, x_test, y_train, y_test = train_test_split(Data_normalized, label_values, test_size=0.4)

# классификаторы и их обучение
from sklearn import naive_bayes
from sklearn.svm import LinearSVC

clf = naive_bayes.MultinomialNB()
svm = LinearSVC()
model1 = clf.fit(x_train, y_train)
model2 = svm.fit(x_train, y_train)

prediction = dict()
prediction["Naive_Bayes"] = model1.predict(x_test)
prediction["SVM"] = model2.predict(x_test)

# вывод в консоль результатов
from sklearn.metrics import classification_report
report = classification_report(y_test, prediction["Naive_Bayes"], target_names=["anom", "norm"])
print(report)

report = classification_report(y_test, prediction["SVM"], target_names=["anom", "norm"])
print(report)

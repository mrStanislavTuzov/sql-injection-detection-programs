import re

from reserved_words import symbols, reserved_words

def get_brackets(str):
    brackets = []
    symbols = ['(', ')', '/*', '*/']
    index = 0
    for symbol in str:
        if symbol in symbols:
            brackets.append({'symbol': symbol, 'index': index})
        index += 1
    return brackets

# удаление из data
def remove_from_data(index, data):
    try:
        result = data[:index] + ' ' + data[(index+1):]
    except:
        result = data[:index]
    return result

# удаление парных скобок
def remove_paired_brackets(data):
    brackets = get_brackets(data)
    index = 0
    while index < len(brackets) - 1:
        if brackets[index]['symbol'] == '(' and brackets[index+1]['symbol'] == ')':
            data = remove_from_data(brackets[index]['index'], data)
            data = remove_from_data(brackets[index+1]['index'], data)
            brackets.pop(index)
            brackets.pop(index)
            index = 0
        else:
            index += 1
    return data

# шестнадцатеричное число -> HEX
# чисто с плавающей точкой -> DEC
# целое -> INT
# символ -> CHR
# строка -> STR
def change_token(token):
    if token.upper() in reserved_words: return token.upper()

    if re.match(r'0[xX][0-9a-fA-F]+', token) is not None: return 'HEX'
    elif re.match(r'\d+\.\d+', token) is not None: return 'DEC'
    elif re.match(r'\d+', token) is not None: return 'INT'
    elif re.match(r'[a-zA-Z]', token) is not None and len(token) == 1: return 'CHR'
    else: return 'STR'

# меняем все символы на определенные слова
def change_symbol(tokens):
    new_tokens = tokens
    index = 0
    for token in new_tokens:
        for i in range(0, len(symbols)):
            if token == symbols[i]['key']:
                new_tokens[index] = symbols[i]['value']
                break
        index += 1
    return new_tokens

def add_space(data):
    result = data
    for letter in result:
        for symbol in symbols:
            if letter == symbol['key']:
                string = ' ' + letter + ' '
                result = result.replace(letter, string)
    return result

def get_normalized_string(data):
    data = add_space(data).replace('`','')
    data = remove_paired_brackets(data)
    tokens = data.split()
    tokens = [change_token(token) for token in tokens]
    new_tokens = change_symbol(tokens)
    return ' '.join(new_tokens)

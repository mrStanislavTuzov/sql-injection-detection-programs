import numpy as np
import pandas as pd
from sklearn.metrics import classification_report

from findSQLIA import *

data = pd.read_csv("../data/data.csv")
data.loc[data["label"]=="norm", "label",] = 1
data.loc[data["label"]=="anom", "label",] = 0

data["detect"] = data["payload"].apply(detect_SQLIA1)

data.loc[data["detect"]==1, "detect"] = data.loc[data["detect"]==1, "payload"].apply(detect_SQLIA2)

data.loc[data["detect"]==1, "detect"] = data.loc[data["detect"]==1, "payload"].apply(detect_SQLIA3)

label = np.array(list(data["label"].values))
detect = data["detect"].values

report = classification_report(label, detect, target_names=["anom", "norm"])
print(report)

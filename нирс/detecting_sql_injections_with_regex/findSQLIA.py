import re

# РВ - регулярное выражение 
# РВ для определения метасимволов
def detect_SQLIA1(text):
    if re.search(r"(\%27)|(\')|(\-\-)|(\%23)|(#)/ix", text) == None:
        return 1
    return 0

# модифицированное РВ для обнаружения SQL метасимволов
def detect_SQLIA2(text):
    if re.search(r"(\%3D|=)[^\n]*(\%27|\')|(\-\-)|(\%3B)|(;)", text) == None:
        return 1
    return 0

# РВ для определения SQL инъекций с использованием ключевых слов
def detect_SQLIA3(text):
    result = re.search(r"(((\%27)|(\'))\s{0,}union)|(((\%27)|(\'))\s{0,}select)|(((\%27)|(\'))\s{0,}insert)|(((\%27)|(\'))\s{0,}update)|(((\%27)|(\'))\s{0,}delete)|(((\%27)|(\'))\s{0,}drop)/i", text)
    if result == None:
        return 1
    return 0
